package de.cherriz.training.rest.notiz.client;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class AvailabilityMonitor {

    private final NotizClient client;

    private final int interval;

    public static void main(String args[]) {
        AvailabilityMonitor monitor = new AvailabilityMonitor(args[0], Integer.parseInt(args[1]));
        monitor.start();
    }

    public AvailabilityMonitor(String url, int interval) {
        this.client = new NotizClient(url);
        this.interval = interval;
    }

    public void start() {
        while (true) {
            System.out.println("Status: " + getStatus());
            try {
                Thread.sleep(interval * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String getStatus() {
        try {
            String status = client.systemCheck();
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readTree(status).path("status").asText();
        } catch (InterruptedException | IOException e) {
            return "Nicht erreichbar";
        }
    }

}