package de.cherriz.training.rest.notiz.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

public class NotizClient {

    private final ObjectMapper mapper;

    private final String server;

    private final HttpClient client;

    public NotizClient(String server) {
        this.mapper = new ObjectMapper();
        this.server = server;
        this.client = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
    }

    public String systemCheck() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder(URI.create(server + "/notiz/systemcheck")).GET().build();
        HttpResponse<String> response = this.client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    public List<Long> getAll() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder(URI.create(server + "/notiz/")).GET().build();
        HttpResponse<String> response = this.client.send(request, HttpResponse.BodyHandlers.ofString());
        return mapper.readValue(response.body(), new TypeReference<List<Long>>() {});
    }

    public Notiz get(Long id) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder(URI.create(server + "/notiz/"+id)).GET().build();
        HttpResponse<String> response = this.client.send(request, HttpResponse.BodyHandlers.ofString());
        return this.mapper.readValue(response.body(), Notiz.class);
    }

    public Notiz create(Notiz notiz) throws IOException, InterruptedException {
        String body = this.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(notiz);
        HttpRequest request = HttpRequest.newBuilder(URI.create(server + "/notiz/create"))
                                         .header("Content-Type", "application/json")
                                         .POST(HttpRequest.BodyPublishers.ofString(body)).build();


        HttpResponse<String> response = this.client.send(request, HttpResponse.BodyHandlers.ofString());
        return this.mapper.readValue(response.body(), Notiz.class);
    }

    public void delete(Long id) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder(URI.create(server + "/notiz/"+id)).DELETE().build();
        this.client.send(request, HttpResponse.BodyHandlers.ofString());
    }


}