package de.cherriz.training.rest.notiz.client;

import java.io.IOException;

public class NotizCreator {

    private final NotizClient client;

    public static void main(String args[]) throws IOException, InterruptedException {
        NotizCreator creator = new NotizCreator(args[0]);
        Notiz notiz = creator.erzeugeNotiz(args[1], args[2]);
        System.out.println("Notiz angelegt mit ID: " + notiz.getId());
    }

    public NotizCreator(String url) {
        this.client = new NotizClient(url);
    }
    public Notiz erzeugeNotiz(String title, String content) throws IOException, InterruptedException {
        Notiz notiz = new Notiz();
        notiz.setTitel(title);
        notiz.setInhalt(content);
        return this.client.create(notiz);
    }

}